package com.myrewards.bayford.three.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.NYXDigital.NiceSupportMapFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.myrewards.bayford.three.utils.Utility;

public class MyRoadsideAssist extends FragmentActivity implements
		LocationListener, android.location.LocationListener, OnClickListener {
	Button backButton, searchBtn, scanBarBtn;
	EditText locationDescText;
	GoogleMap googleMap;
	Marker startPerc;
	String str1;
	TextView numberTV, titleTV;
	final private static int CALL_NUMBER = 1;
	Location location;
	
	GPSTracker mGPS = null;

	//Initialize to a non-valid zoom value
		private float previousZoomLevel = -1.0f;
		private boolean isZooming = false;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_roadside_assist);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.my_road_side_assists));

		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);

		LinearLayout mapheader = (LinearLayout) findViewById(R.id.mapParentLLID);
		mapheader.getLayoutParams().height = (int) (Utility.screenHeight / 10.5);

		backButton = (Button) findViewById(R.id.backBtnID);
		backButton.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		searchBtn = (Button) findViewById(R.id.searchRoadLocID);
		locationDescText = (EditText) findViewById(R.id.myLoactionETID);
		locationDescText.setTypeface(Utility.font_reg);
		numberTV = (TextView) findViewById(R.id.roadContactNumberTVID);

		backButton.setOnClickListener(this);
		searchBtn.setOnClickListener(this);
		numberTV.setOnClickListener(this);
		
		// Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available
        	
        	int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
            
        }else {	// Google Play Services are available
        	try {
    			initilizeMaps();
    		} catch (Exception e) {
    			Log.w("Hari--->", e);
    		}
        }		
	}

	private void initilizeMaps() {
		if (googleMap == null) {
			NiceSupportMapFragment mapFragment = (NiceSupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.mapfragmentId);
			googleMap = mapFragment.getMap();
			mapFragment.setPreventParentScrolling(false);
		}

		// check if map is created successfully or not
		if (googleMap == null) {
			Toast.makeText(getApplicationContext(),
					"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
		} else {
			googleMap.setMyLocationEnabled(true);

			if (mGPS == null) {
				mGPS = new GPSTracker(MyRoadsideAssist.this);
			}

			// check if mGPS object is created or not
			if (mGPS != null && location == null) {
				location = mGPS.getLocation();
			}

			// check if location is created or not
			if (location != null) {
				googleMap.setMyLocationEnabled(true);
				onLocationChanged(location);
			} else {
				showDialog(1207);
			}
		}
	}

	@SuppressWarnings("unused")
	@Override
	public void onLocationChanged(Location arg0) {
		try {
			if (arg0 != null) {
				try {
					LatLng yyy = new LatLng(arg0.getLatitude(), arg0.getLongitude());
					startPerc = googleMap.addMarker(new MarkerOptions().position(yyy)
							.title("I am Here").snippet("")
							
							.icon(BitmapDescriptorFactory.fromResource(R.drawable.me)));

					LatLng latLng = new LatLng(arg0.getLatitude(), arg0.getLongitude());
					CameraPosition cameraPosition = new CameraPosition.Builder()
							.target(latLng) // Sets the center of the map to Mountain View
							.zoom(12) // Sets the zoom
							.bearing(90) // Sets the orientation of the camera to east
							.tilt(30) // Sets the tilt of the camera to 30 degrees
							.build(); // Creates a CameraPosition from the builder

					CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 12);
					
					// Showing the current location in Google Map
					googleMap.moveCamera(cameraUpdate);

					// Zoom in the Google Map
					googleMap.animateCamera(cameraUpdate);
					googleMap.setOnCameraChangeListener(getCameraChangeListener());
					Geocoder geo = new Geocoder(
							MyRoadsideAssist.this.getApplicationContext(),
							Locale.getDefault());
					try {
						List<Address> ads = geo.getFromLocation(arg0.getLatitude(),
								arg0.getLongitude(), 1);

						String s1 = ads.get(0).getSubLocality(), s2 = ads.get(0)
								.getLocality(), s3 = ads.get(0).getSubAdminArea(), s4 = ads
								.get(0).getAdminArea(), s5 = ads.get(0).getCountryName();
						String str[] = { s1, s2, s3, s4, s5 };

						str1 = "";
						int count = 0;
						for (int i = 0; i < str.length; i++) {
							if (str[i] != null) {
								if (str1 == "") {
									str1 = str[i];
								} else {
									str1 = str1 + "," + str[i];
								}
								count++;
							}
							if (count == 2) {
								break;
							}
						}
						locationDescText.setText(str1);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (Exception e) {
					Log.w("Hari--->", e);
				}
			}
		} catch (Exception e) {
			Log.w("Hari--->", e);
		}
	}
	public OnCameraChangeListener getCameraChangeListener()
	{
	    return new OnCameraChangeListener() 
	    {
	        @Override
	        public void onCameraChange(CameraPosition position) 
	        {
	            Log.d("Zoom", "Zoom: " + position.zoom);

	            if(previousZoomLevel != position.zoom)
	            {
	                isZooming = true;
	            }

	            previousZoomLevel = position.zoom;
	        }
	    };
	}
	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.backBtnID:
			finish();
			break;
		case R.id.searchRoadLocID:
			try {
				locationDescText.setText(str1);
			} catch (Exception e) {
				Log.w("Hari-->", e);
			}
			break;
		case R.id.roadContactNumberTVID:
			try {
				call(numberTV.getText().toString());
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
			break;

		default:
			break;
		}
	}

	private void call(String string) {
		showDialog(CALL_NUMBER);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 1) {
				AlertDialog callOneDialog = null;
				switch (id) {
				case CALL_NUMBER:
					LayoutInflater callInflater1 = LayoutInflater.from(this);
					View callOneView1 = callInflater1.inflate(
							R.layout.dialog_layout_call_one, null);
					AlertDialog.Builder callAlert1 = new AlertDialog.Builder(this);
					callAlert1.setView(callOneView1);
					callOneDialog = callAlert1.create();
					break;
				}
				return callOneDialog;
			} else if (id == 1207) {
				AlertDialog GPSAlert = null;
				LayoutInflater liDelete = LayoutInflater
						.from(MyRoadsideAssist.this);
				View deleteFavView = liDelete.inflate(
						R.layout.dialog_layout_delete_favorite, null);
				AlertDialog.Builder adbDeleteFav = new AlertDialog.Builder(
						MyRoadsideAssist.this);
				adbDeleteFav.setView(deleteFavView);
				GPSAlert = adbDeleteFav.create();
				return GPSAlert;
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			if (id == 1) {
				switch (id) {
				case CALL_NUMBER:
					final AlertDialog alertDialog1 = (AlertDialog) dialog;

					TextView alertTitle = (TextView) alertDialog1
							.findViewById(R.id.firstLoginCEPUTitleTVID);
					alertTitle.setTypeface(Utility.font_bold);
					TextView tv11 = (TextView) alertDialog1
							.findViewById(R.id.callNumberOneTVID);
					tv11.setTypeface(Utility.font_reg);
					tv11.setText(numberTV.getText().toString());
					Button yesBtn = (Button) alertDialog1
							.findViewById(R.id.call_one_yesBtnID);
					yesBtn.setTypeface(Utility.font_bold);
					Button noBtn = (Button) alertDialog1
							.findViewById(R.id.call_one_noBtnID);
					noBtn.setTypeface(Utility.font_bold);
					yesBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							try {
								Intent intent = new Intent(Intent.ACTION_CALL);
								intent.setData(Uri.parse("tel:1800-244-615"));
								startActivity(intent);
								alertDialog1.dismiss();
							} catch (Exception e) {
								Log.w("Hari-->", e);
							}
						}
					});
					noBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							alertDialog1.dismiss();
						}
					});
					break;
				}
			} else if (id == 1207) {
				final AlertDialog alt3 = (AlertDialog) dialog;
				TextView alertTilteTV = (TextView) alt3.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTilteTV.setTypeface(Utility.font_bold);
				alertTilteTV.setText("GPS Settings !");
				TextView tv22 = (TextView) alt3.findViewById(R.id.deleteFavTVID);
				tv22.setTypeface(Utility.font_reg);
				tv22.setText("GPS is not enabled. Do you want to go to settings menu?");
				Button deleteFavYesBtn = (Button) alt3.findViewById(R.id.delete_fav_yesBtnID);
				deleteFavYesBtn.setTypeface(Utility.font_bold);
				deleteFavYesBtn.setText("Settings");
				Button deleteNoFavBtn = (Button) alt3
						.findViewById(R.id.delete_fav_noBtnID);
				deleteNoFavBtn.setTypeface(Utility.font_bold);
				deleteNoFavBtn.setText("Cancel");
				deleteFavYesBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							Intent intent = new Intent(
									android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							MyRoadsideAssist.this.startActivity(intent);
							MyRoadsideAssist.this.finish();
						} catch (Exception e) {
							Log.w("Hari-->", e);
						}
						alt3.dismiss();
					}
				});
				deleteNoFavBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						alt3.dismiss();
					}
				});
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}
}
