package com.myrewards.bayford.three.service;

public interface BayfordServiceListener {
	public void onServiceComplete(Object response, int eventType);
}
