package com.myrewards.bayford.three.service;

public interface BayfordNetworkListener {
	void onRequestCompleted(String response, String errorString, int eventType);

}
